import React, { Component } from 'react';

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.handleSearch(this.state.value);
    }

    render() {
        return (
            <div className="container">
                <div className="jumbotron py-5 my-5">
                    <form className="form-inline search-form" onSubmit={this.handleSubmit}>
                        <input className="form-control" type="text" value={this.state.value} onChange={this.handleChange} />

                        <input type="submit" className="btn btn-primary" value="Search" />
                    </form>
                </div>
            </div>
        );
    }
}

export default Form;
