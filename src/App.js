import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Form from './Form';
import List from './Books';

const key = 'AIzaSyC5Xlpkh22PWHDb5BTq6lnNVagT2kBuMYI';
const api = 'https://www.googleapis.com/books/v1/volumes';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: 'react',
            index: 0,
            books: null,
            load: 1,
        };

        this.handleForm = this.handleForm.bind(this);

        this.searchBooks = this.searchBooks.bind(this);

        this.handleScroll = this.handleScroll.bind(this);

        this.moreBooks = this.moreBooks.bind(this);
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll);

        this.searchBooks(this.state.search, this.state.index);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
        const windowHeight = 'innerHeight' in window ? window.innerHeight : document.documentElement.offsetHeight;

        const body = document.body;

        const html = document.documentElement;

        const docHeight = Math.max(
          body.scrollHeight,
          body.offsetHeight,
          html.clientHeight,
          html.scrollHeight,
          html.offsetHeight,
        );

        const windowBottom = windowHeight + window.pageYOffset;

        if (windowBottom >= docHeight) {
            this.moreBooks();
        }
    }

    handleForm(e) {
        this.setState({
            search: e,
            index: 0,
        });

        this.searchBooks(e, 0);
    }

    moreBooks() {
        const idx = this.state.index + 10;

        const q = `${api}?q=${this.state.search}&startIndex=${idx}&key=${key}`;

        fetch(q)
            .then(res => res.json())
            .then((result) => {
                const res = this.state.books;

                if (res.items.length < res.totalItems) {
                    res.items = res.items.concat(result.items);

                    this.setState({
                        books: res,
                        index: idx,
                    });
                }
            });
    }

    searchBooks(e, i) {
        this.setState({ load: 1 });

        const q = `${api}?q=${e}&startIndex=${i}&key=${key}`;

        fetch(q)
            .then(res => res.json())
            .then((result) => {
                this.setState({
                    books: result,
                    load: 0,
                });
            });
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Welcome to React</h2>
                </div>

                <Form handleSearch={this.handleForm} />

                <List books={this.state.books} load={this.state.load} />
            </div>
        );
    }
}

export default App;
