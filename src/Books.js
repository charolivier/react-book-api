import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const List = (props) => {
    const res = [];
    let load = null;
    let key = 0;

    if (props.load) {
        load = (<p className="d-block mx-auto">Loading..</p>);
    } else if (props.books) {
        props.books.items.forEach((e) => {
            let title;
            let img;

            key += 1;

            if (e.volumeInfo.title && e.volumeInfo.title.toLowerCase() !== 'undefined') {
                title = <h4 className="card-title">{e.volumeInfo.title}</h4>;
            }

            if (e.volumeInfo.imageLinks && e.volumeInfo.imageLinks.thumbnail) {
                img = <img className="card-img-top img-fluid" src={e.volumeInfo.imageLinks.thumbnail} alt={title} />;
            }

            res.push(
                <div className="card" id={key} key={key}>
                    <a href={e.volumeInfo.infoLink} target="_blank">{img}</a>
                </div>,
              );
        });
    }

    return (
        <div className="container">
            {load}
            <div className="card-columns">
                <ReactCSSTransitionGroup
                    transitionName="card"
                    transitionEnterTimeout={1000}
                    transitionLeaveTimeout={50}>
                    {res}
                </ReactCSSTransitionGroup>
            </div>
        </div>
    );
};

export default List;
