# webpack react demo

## install
yarn --development or npm install --development 

## Start
npm start

## Build
npm run build

## Dev
At times the server will not start
run:	
$ lsof -i tcp:9000
$ lill -9 <id>

## Features
Ajax
Text input binding
Child var to parent 
React animation
Lazy load

## Source
api: https://developers.google.com/books/docs/overview
